# ABI_rgb_fire_aod

## Name
ABI_rgb_fire_aod

## Description
These five jupyter notebooks were made for the GOES DataJam 2023. This cover accessing NOAA GOES ABI data from AWS, constructing RGB imagery (true color and fire temperature), the fire classification product, and the AOD product. This code was made for education purpose and to spread awareness of the ABI satellite tools.


## Support
Email: stie311@gmail.com


## Authors and acknowledgment
Author: Tyler C. Summers

### Learn More
- [GOES ABI Bands Quick Information Guides](https://www.goes-r.gov/mission/ABI-bands-quick-info.html)
- [CIRA Regional and Mesoscale Meteorology Branch Resources](https://rammb2.cira.colostate.edu/training/visit/quick_reference/#tab17)
- [GOES 2-go: GOES ABI RGB Recipes, by Brain Blaylock (2019)](https://blaylockbk.github.io/goes2go/_build/html/user_guide/notebooks/DEMO_rgb_recipes.html)
- Cooperative Institute for Meteorological Satellite Studies (CIMSS)
 - [Quick Guides](https://cimss.ssec.wisc.edu/goes/OCLOFactSheetPDFs/)
 - [GOES Activities](https://cimss.ssec.wisc.edu/goes/)
- [Beginner’s Guide to GOES-R Series Data, compiled by Danielle Losos](https://www.goes-r.gov/downloads/resources/documents/Beginners_Guide_to_GOES-R_Series_Data.pdf)

### References
- Lindsey, D and Miller, S. (2019). Satellite Imagery RGBs: Adding Value, Saving Time. NOAA NESDIS. URL: https://www.nesdis.noaa.gov/news/satellite-imagery-rgbs-adding-value-saving-time.
- Schmit, T. J. and Gunshor, M. M. (2019). Chapter 4 – ABI Imagery from the GOES-R Series. The GOES-R Series A New Generation of Geostationary Environmental Satellites. https://doi.org/10.1016/B978-0-12-814327-8.00004-4.


